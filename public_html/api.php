<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
set_time_limit ( 60 * 15 ) ; // Seconds

require_once ( 'php/oauth.php' ) ;
require_once ( 'php/common.php' ) ;

$datadir = '/data/project/tooltranslate/public_html/data' ;
$mode = get_request ( 'mode' , '' ) ;
$db = openToolDB ( 'tooltranslate_p' ) ;
$db->set_charset("utf8") ;
$oa = new MW_OAuth ( 'tooltranslate' , 'commons' , 'wikimedia' ) ;

function generateToolinfo ( $id ) {
	global $db , $datadir ;

	$toolname = '' ;
	$sql = "SELECT * FROM tool WHERE id=$id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $toolname = $o->name ;

	if ( $toolname == '' ) return '' ;

	$sql = "SELECT DISTINCT language FROM translation WHERE tool_id=$id AND current=1 ORDER BY language" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$j = array('languages'=>array(),'meta'=>array()) ;
	while($o = $result->fetch_object()){
		$j['languages'][] = $o->language ;
	}
	
	$sql = "SELECT * FROM tool WHERE id=$id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $j['meta'][$k] = $v ;
	}
	
	$j = json_encode ( $j ) ;
	$dir = "$datadir/$toolname" ;
	if ( !file_exists ( $dir ) ) mkdir ( $dir ) ;
	$path = "$dir/toolinfo.json" ;
	file_put_contents ( $path , $j ) ;
	
	return $path ;
}

function generateJSON ( $toolname , $language ) {
	global $db , $datadir ;
	$id = 0 ;
	$sql = "SELECT * FROM tool WHERE name='" . $db->real_escape_string ( $toolname ) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $id = $o->id ;
	if ( $id == 0 ) return ;

	$language = strtolower ( preg_replace ( '/[^a-z_-]/' , '' , $language ) ) ;
	$j = array() ;
	$sql = "SELECT * FROM translation WHERE tool_id=$id AND language='$language' AND current=1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$j[$o->key] = json_decode ( $o->json ) ;
	}
	$j = json_encode ( (object) $j ) ;
	$dir = "$datadir/$toolname" ;
	if ( !file_exists ( $dir ) ) mkdir ( $dir ) ;
	$path = "$dir/$language.json" ;
	file_put_contents ( $path , $j ) ;
	generateToolinfo ( $id ) ;
	return $j ;
}




$out = array() ;
if ( $mode == 'get_tools' ) {
	$sql = "SELECT * FROM tool" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][] = $o ;
	}
} else if ( $mode =='get_tool_info' ) {
	$id = get_request ( 'id' , '0' ) * 1 ;
	$filename = generateToolinfo ( $id ) ;
	$out['data'] = json_decode ( file_get_contents ( $filename ) ) ;
/*	$sql = "SELECT DISTINCT language FROM translation WHERE tool_id=$id AND current=1 ORDER BY language" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$out['data'] = array() ;
	while($o = $result->fetch_object()){
		$out['data']['languages'][] = $o->language ;
	}*/
} else if ( $mode =='update_json' ) {
	$toolname = get_request ( 'toolname' , '' ) ;
	$language = get_request ( 'language' , '' ) ;
	if ( $language == '' ) {
		$sql = "SELECT DISTINCT language FROM tool,translation WHERE name='".$db->real_escape_string($toolname)."' AND tool_id=tool.id AND current=1 ORDER BY language" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$langs = array() ;
		while($o = $result->fetch_object()) $langs[] = $o->language ;
		foreach ( $langs AS $l ) $out['data'][] = generateJSON ( $toolname , $l ) ;
	} else {
		$out['data'] = generateJSON ( $toolname , $language ) ;
	}
} else if ( $mode =='suggest_translations' ) {
	$language = get_request ( 'language' , '' ) ;
	$key = get_request ( 'key' , '' ) ;
	$skip_tool = get_request ( 'skip_tool' , '0' ) * 1 ;
	$language = strtolower ( preg_replace ( '/[^a-z_-]/' , '' , $language ) ) ;
	$sql = "SELECT DISTINCT json FROM translation WHERE language='$language' AND `key`='" . $db->real_escape_string($key) . "' AND current=1 AND tool_id != $skip_tool" ;
	$out['data'] = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][] = json_decode ( $o->json ) ;
	}


} else if ( isset($_REQUEST['oauth_token']) ) {

	print get_common_header ( '' , "ToolTranslate" ) ;
	if ( $oa->isAuthOK() ) {
		print "<p>You have been successfully authenticated! Close this tab and reload the original one.</p>" ;
	} else {
		print "<p>Something went wrong with your authentication:<br/>" . $oa->error . "</p>" ;
	}

} else if ( $mode == 'logout' ) {

	$oa->logout() ;
	print get_common_header ( '' , "ToolTranslate" ) ;
	print "<p>You have been logged out of the app.</p>" ;

} else if ( $mode == 'check_auth' ) {

	if ( $oa->isAuthOK() ) {
		$out['logged_in'] = 1 ;
		$out['data'] = $oa->getConsumerRights() ;
	} else {
		$out['logged_in'] = 0 ;
		$out['data'] = $oa->error ;
	}
	
} else if ( $mode == 'authorize' ) {

	$oa->doAuthorizationRedirect('api.php');

} else if ( $mode == 'logout' ) {

	$oa->logout();

} else if ( $mode == 'add_tool' ) {
	
	$name = $db->real_escape_string ( trim(get_request ( 'name' , '' )) ) ;
	$label = $db->real_escape_string ( trim(get_request ( 'label' , '' )) ) ;
	$url = $db->real_escape_string ( trim(get_request ( 'url' , '' )) ) ;

	$tool_id = 0 ;
	$sql = "SELECT * FROM tool WHERE name='$name'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $tool_id = $o->id ;

	if ( $oa->isAuthOK() and $name != '' and $tool_id == 0 ) {
		$auth = $oa->getConsumerRights() ;
		$username = $db->real_escape_string ( $auth->query->userinfo->name ) ;
		$sql = "INSERT INTO tool (name,label,url,owner) VALUES ('$name','$label','$url','$username')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$tool_id = $db->insert_id ;
		$out['data']['tool'] = $tool_id ;
		generateToolinfo ( $tool_id ) ;
	}
	
} else if ( $mode == 'store_translation' ) {

	$language = get_request ( 'language' , '' ) ;
	$language = $db->real_escape_string ( strtolower ( preg_replace ( '/[^a-z_-]/' , '' , $language ) ) ) ;
	$key = get_request ( 'key' , '' ) ;
	$tool = get_request ( 'tool' , '0' ) * 1 ;
	$json = json_decode ( get_request ( 'json' , '' ) ) ;

	if ( $oa->isAuthOK() ) {
		
		$auth = $oa->getConsumerRights() ;
		$username = $auth->query->userinfo->name ;
		$key = $db->real_escape_string ( $key ) ;
		$json = $db->real_escape_string ( json_encode($json) ) ;
		$ts = date ( 'YmdHis' ) ;
		
		$out['data'] = 'OK' ;
		$db->autocommit(FALSE);
		$sql = "UPDATE translation SET current=0 WHERE current=1 AND tool_id=$tool AND `key`='$key' AND language='$language'" ;
		$out['sql'][] = $sql ;
		$db->query($sql) ;
//		if(!$result = $db->query($sql)) $out['data'] = '1: There was an error running the query [' . $db->error . ']' ;
		$sql = "INSERT INTO translation (tool_id,language,`key`,json,user,timestamp,current) VALUES ($tool,'$language','$key','$json','$username','$ts',1)" ;
		$out['sql'][] = $sql ;
//		if(!$result = $db->query($sql)) $out['data'] = '2: There was an error running the query [' . $db->error . ']';
		$db->query($sql) ;
		if ( !$db->commit() ) $out['data'] = '3: There was an error running the query [' . $db->error . ']';
		$db->autocommit(TRUE);

		$toolname = '' ;
		$sql = "SELECT * FROM tool WHERE id=$tool" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()) $toolname = $o->name ;
		if ( $toolname != '' ) generateJSON ( $toolname , $language ) ;
		
	} else {
		$out['data'] = "ERROR: Not logged in. " . $oa->error ;
	}

	
} else if ( $mode =='update_languages' ) { // Do this manually, occasionally; shouldn't change a lot...
	$sparql = 'SELECT DISTINCT ?q ?code ?label { ?q wdt:P31/wdt:P279* wd:Q34770 ; wdt:P424 ?code ; rdfs:label ?label . FILTER(LANG(?label) = ?code) } ORDER BY ?code' ;
	$j = getSPARQL ( $sparql ) ;
	$out['data'] = array() ;
	if ( !isset($j->results) or !isset($j->results->bindings) or count($j->results->bindings) == 0 ) return $ret ;
	foreach ( $j->results->bindings AS $v ) {
		$out['data'][$v->code->value] = $v->label->value ;
	}
	$path = "$datadir/languages.json" ;
	$j = json_encode ( $out['data'] ) ;
	file_put_contents ( $path , $j ) ;


} else if ( $mode == 'undo' ) { // INCOMPLETE

	$translation_id = get_request ( 'translation_id' , '0' ) * 1 ;
	$orig = (object) array() ;
	$sql = "SELECT * FROM translation WHERE id=$translation_id" ;
	$out['sql'][] = $sql ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $orig = $o ;
	
	$sql = "SELECT id FROM translation WHERE tool_id='{$orig->tool_id}' AND current=0 AND language='" . $db->real_escape_string($orig->language) . "' AND `key`='" . $db->real_escape_string($orig->key) . "'" ;
	$sql .= " ORDER BY TIMESTAMP DESC LIMIT 1" ;
	$out['sql'][] = $sql ;
	$last = (object) array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $last = $o ;
	$out['data'] = $last ;
	

} else if ( $mode == 'rc' ) {

	// TODO user
	$offset = get_request ( 'offset' , '0' ) * 1 ;
	$limit = get_request ( 'limit' , '50' ) * 1 ;
	$tool = get_request ( 'tool' , '0' ) * 1 ;
	$lang = $db->real_escape_string ( strtolower ( trim ( get_request ( 'lang' , '' ) ) ) ) ;
	
	$sql = "SELECT * FROM translation WHERE 1=1" ;
	if ( $tool != 0 ) $sql .= " AND tool_id=$tool" ;
	if ( $lang != '' ) $sql .= " AND language='$lang'" ; 
	$sql .= " ORDER BY timestamp DESC LIMIT $limit OFFSET $offset" ;
	$out['sql'][] = $sql ;
	
	$out['data'] = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $out['data'][] = $o ;
	

} else if ( $mode == 'oneoff' ) {
	header('Content-type: text/plain; charset=UTF-8');
/*
	$rows = explode ( "\n" , file_get_contents ( "../petscan_dump" ) ) ;
	foreach ( $rows AS $row ) {
		$r = explode ( "\t" , $row , 3 ) ;
		$sql = "INSERT IGNORE INTO translation (tool_id,language,`key`,json,user,timestamp,current) VALUES (11,'{$r[0]}','" ;
		$sql .= $db->real_escape_string($r[1]) . "','" . $db->real_escape_string ( json_encode ( $r[2] ) ) . "'," ;
		$sql .= "'Magnus Manske','20160722130800',1)" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	}
*/
	exit(0);
} else {
	$out['data'] = "ERROR" ;
}


if ( isset($out['data']) ) {
	header('Content-type: text/plain; charset=UTF-8');
	print json_encode ( $out ) ;
	exit ( 0 ) ;
}

?>